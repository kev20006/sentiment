export const stub1 = {
	"Score":{
		"Positive": 0.03,
		"Negative": 0.17,
		"Neutral": 0.8
	},
	"KeyPhrases":[
		"Bullshit",
		"Layer",
		"Awful"
	],
	"Sentances":[
		{
			"Sentance":"This was my sentance",
			"Score":{
				"Positive": 0.15,
				"Negative": 0.30,
				"Neutral": 0.6
			}
		},
        {
			"Sentance":"I HATE THIS",
			"Score":{
				"Positive": 0.20,
				"Negative": 0.50,
				"Neutral": 0.30
			}
		},
        {
			"Sentance":"This is the best thing ever!!",
			"Score":{
				"Positive": 0.66,
				"Negative": 0.33,
				"Neutral": 0.33
			}
		}
	]
}