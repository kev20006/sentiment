import React from 'react';

import { stub1 } from '../data/testdata';


export const SentimentalTable = () => {

    const cellStyles = {border: '0.5px solid hotpink', padding: '5px 10px'}

    const tableRow = (sentence: string, positive: number, negative: number, neutral: number, isEven: boolean) => (
        <div style={{display: 'flex', background: isEven ? 'white' : '#f9d8e882'}}>
            <span style={{flex:1, textAlign: 'left', ...cellStyles}}>{sentence}</span>
            <span style={{width:'15%', ...cellStyles}}>{Math.floor(positive * 100)}%</span>
            <span style={{width:'15%', ...cellStyles}}>{Math.floor(negative * 100)}%</span>
            <span style={{width:'15%', ...cellStyles}}>{Math.floor(neutral * 100)}%</span>
        </div>
    )

    return (
        <div style={{border: '1px solid hotpink'}}>
            <div style={{display: 'flex'}}>
                <span style={{flex:1, textAlign: 'left', ...cellStyles, fontWeight: 600}}>Sentence</span>
                <span style={{width:'15%', ...cellStyles}}>😀</span>
                <span style={{width:'15%', ...cellStyles}}>😡</span>
                <span style={{width:'15%', ...cellStyles}}>😐</span>
            </div>
            {stub1.Sentances.map((sentence, idx) => tableRow(sentence.Sentance, sentence.Score.Positive, sentence.Score.Negative, sentence.Score.Neutral, !!(idx % 2)))}
        </div>
       
    )
}