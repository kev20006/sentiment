import React from 'react';
import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend,
} from 'chart.js';
import { Radar } from 'react-chartjs-2';
import { stub1 } from '../data/testdata';

ChartJS.register(
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend
);

function generateColor(positive: number, negative: number, neutral: number, opacity=1) {
    return `rgba(${negative * 255}, ${positive * 255}, ${neutral * 255}, ${opacity})`
}



export const data = {
  labels: ['Positive', 'Negative', 'Neutral'],
  datasets: stub1.Sentances.map((sentence) => ({
    label: sentence.Sentance,
    data: [sentence.Score.Positive * 100, sentence.Score.Negative * 100, sentence.Score.Neutral * 100],
    backgroundColor: generateColor(sentence.Score.Positive, sentence.Score.Negative, sentence.Score.Neutral, 0.2),
    borderColor: generateColor(sentence.Score.Positive, sentence.Score.Negative, sentence.Score.Neutral, 1),
    borderWidth: 1,
  })) 
};

export function SentimentalRadar() {
  return (
    <div className='radar-wrapper' style={{width: '600px', height: '600px', margin: '0 auto'}}>
        <Radar data={data} />
    </div>
    
  );
}
