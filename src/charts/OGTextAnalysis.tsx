import React from 'react';
import ReactDOMServer from 'react-dom/server';

import { stub1 } from '../data/testdata';

interface Sentence {
    Sentance: string;
    Score: {
        Positive: number;
        Negative: number;
        Neutral: number;
    };
}

export const OGTextAnalysis = () => {

    const message = '<p>This was my sentance. I HATE THIS. <br /> This is the best thing ever!!<p>'

    function generateColor(positive: number, negative: number, neutral: number) {
        return (`rgb(${negative * 255}, ${positive * 255}, ${neutral * 255})`)
    }

    function getMarkUp(sentence: Sentence){
        return  ReactDOMServer.renderToString(
            <span className='sentenceHighlight' style={{color: generateColor(sentence.Score.Positive, sentence.Score.Negative, sentence.Score.Neutral)}}>
                <span className='tooltip'>
                    <span>😀: {Math.floor(sentence.Score.Positive*100)}%</span>
                    <span>😡: {Math.floor(sentence.Score.Negative*100)}%</span>
                    <span>😐: {Math.floor(sentence.Score.Neutral*100)}%</span>
                </span>
                {sentence.Sentance}
            </span>)
    }

    function formatMesage(){
        let newMessage = message
        stub1.Sentances.forEach(sentence => {
            newMessage = newMessage.replaceAll(sentence.Sentance, getMarkUp(sentence).toString())
        })
        return newMessage
    } 

    return (
        <>
            <p>Mouse over phrases to see sentiment for specific sentences</p>
            <div style={{position: 'relative', border: '1px solid hotpink', borderRadius: '4px', width: '80%', margin: '0 auto', minHeight: '600px', padding: '15px', background: 'rgb(250,250,250)'}}>
                <span style={{position:'absolute', top: '-12px', padding: '0 5px', background: 'white', left: '10px', color:'hotpink', fontWeight:'600'}}>Original Text</span>
                <div 
                    style={{}}
                    dangerouslySetInnerHTML={{__html: formatMesage()}}
                />   
            </div>
             
        </>
       
    )
}