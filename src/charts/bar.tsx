import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { stub1 } from '../data/testdata';


ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const,
    },
    title: {
      display: true,
      text: 'Overall Sentiment',
    },
  },
};

const labels = ['Sentiment'];

export const data = {
  labels,
  datasets: [
    {
      label: 'Positive',
      data: [stub1.Score.Positive],
      backgroundColor: 'rgba(75, 192, 192, 0.2)',
    },
    {
        label: 'Negative',
        data: [stub1.Score.Negative],
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
    },
    {
        label: 'Neutral',
        data: [stub1.Score.Neutral],
        backgroundColor:'rgba(54, 162, 235, 0.2)'
    }
  ],
};

export function SentimentalBar() {
  return (
    <div className='pie-container' style={{height:'400px', width: '600px'}}>
        <Bar options={options} data={data} />
    </div>
  );
}
