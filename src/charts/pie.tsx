import React from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import { stub1 } from '../data/testdata';

ChartJS.register(ArcElement, Tooltip, Legend);

export const SentimentalPie = () =>  {
    
    const dataSet = {
        labels:  ['Positive', 'Negative', 'Neutral'],
        datasets: [
          {
            label: 'Overall Content Sentiment',
            data: [stub1.Score.Positive, stub1.Score.Negative, stub1.Score.Neutral],
            backgroundColor: [
              'rgba(75, 192, 192, 0.2)',
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(75, 192, 192, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1,
          },
        ],
    }
    

  return (
    <div className='pie-container' style={{height:'400px', width: '400px'}}>
        <Pie data={dataSet}/>
    </div>
  )
  
}
