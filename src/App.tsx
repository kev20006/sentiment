import React, { useState } from 'react';

import './App.css';
import { SentimentalPie } from './charts/pie';
import { SentimentalBar } from './charts/bar';
import { SentimentalRadar } from './charts/radar';
import { OGTextAnalysis } from './charts/OGTextAnalysis'; 
import { SentimentalTable } from './charts/SentimentTable';



function App() {

  const [currentChart, setCurrentChart] =  useState<string>('PIE')
  const [contentChart, setContentChart] =  useState<string>('OG')



  function getButtonStyle(chartName: string, compareChart: string) {
    return {border: 'none', background:'transparent', margin: '0 5px', color: chartName === compareChart ? 'hotpink' : 'blue'}
  }

  return (
    <div className="App" style={{maxWidth: '900px', margin: '0 auto'}}>
      <header>
        <h1>I am Sentimental!!</h1>
      </header>
      <main>
        <section>
          
          <div style={{display:'flex', justifyContent:'space-between', alignItems: 'baseline'}}>
            <h3>Overall Stats</h3>
            <div style={{display:'flex', justifyContent:'center', alignItems: 'baseline'}}>
              <button style={getButtonStyle('PIE', currentChart)} onClick={() => {setCurrentChart('PIE')}}>Pie Chart</button>
              <button style={getButtonStyle('BAR', currentChart)} onClick={() => {setCurrentChart('BAR')}}>Bar Chat</button>
              <button style={getButtonStyle('HIDE', currentChart)} onClick={() => {setCurrentChart('HIDE')}}>Hide</button>
            </div>
            
          </div>
         
          <div style={{display:'flex', justifyContent:'center'}}>
            {(() => {
              switch(currentChart) {
                case 'PIE':
                  return  <SentimentalPie />
                case 'BAR':
                  return <SentimentalBar />
                default:
                  return <></>
              } 
            })()} 
          </div>
         
        </section>
        <section>
        <div style={{display:'flex', justifyContent:'space-between', alignItems: 'baseline'}}>
            <h3>Content Analysis</h3>
            <div style={{display:'flex', justifyContent:'center', alignItems: 'baseline'}}>
              <button style={getButtonStyle('OG', contentChart)} onClick={() => {setContentChart('OG')}}>Original Text Content</button>
              <button style={getButtonStyle('TABLE', contentChart)} onClick={() => {setContentChart('TABLE')}}>Sentence Table</button>
              <button style={getButtonStyle('RADAR', contentChart)} onClick={() => {setContentChart('RADAR')}}>Rader Chart</button>
              <button style={getButtonStyle('HIDE', contentChart)} onClick={() => {setContentChart('HIDE')}}> Hide</button>
            </div>
            
          </div>
         
          <div style={{justifyContent:'center'}}>
            {(() => {
              switch(contentChart) {
                case 'OG':
                  return  <OGTextAnalysis />
                case 'RADAR':
                  return  <SentimentalRadar />
                case 'TABLE':
                  return <SentimentalTable />
                default:
                  return <></>
              } 
            })()} 
            </div>
        </section>
      </main>
    </div>
  );
}

export default App;
